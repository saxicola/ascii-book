== Printing and Binding a Book

You may want to print a dead tree version of your book using a laser (or inkjet but I'm not covering that here) printer.

I sometimes print books for my own use, manuals etc. in A5 format, simply folded and stapled booklet style or bound using plastic comb bindings. Getting the pagination right is sometimes a bit of a head spinner.  The printer I use is a simple A4 non-duplex printer and I need to paginate the book so the output
of the printer is in the correct order to be simply folded, stapled and trimmed without too much (ideally none) manual sorting.
To do do this the pages have to be re-flowed onto A4 sheets in the correct order.

The Linux utilities pdfnup or pdfbook2 can be used for this.  Install texlive-pdfjam for pdfnup and/or texlive-pdfbook2 for pdfbook2 using the appropriate installer for your system.

Make sure your printer is reliable though.  I've had printers that would jam every eleven pages, or double feed, or some other error so that getting an entire book through was a PITA.  I've had to print pages in batches of 10 just in case somewhere in a run of 100 pages it would screw up and waste the entire run of paper.  This was usually when printing A4 pages to be inserted into a ring binder.  Anyway, let's assume you have a reliable printer.

Having produced your initial pdf you need to now shrink and order the pages to correctly print to an A5 booklet.
There are several Linux utilities available for this and I'll show the easiest.

So do:

[source,bash]
pdfbook2 --paper=a4paper book.pdf

You will get an output file named  book-book.pdf

Print this first using even pages, take the bundle from the output tray, and put it back into the input tray, printed side up with the top towards you.  Then print the odd pages.  Depending on how your printer spits out the paper you may need to print either the even OR the odd pages in reverse order.  Experiment with short books and stick a label with instructions on the printer for future reference.  You will have forgotten by the next time you need to do this, trust me.

Fold, staple, profit, retire.

The pdfnup utility seems to require more options as to the pagination required etc. but may offer more control over the output.  Read the manual if you want to use it.

Of course you can combine the entire production into a single line cammand:

[source, bash]
a2x --fop -fpdf --stylesheet=epub.css  book.asc && pdfbook2 --paper=a4paper book.pdf

Larger books will not fold into booklets and the book will either have to split into signatures, which are subsets of the book folded, stitched and then "perfect bound", or the pages guillotined and individually perfect bound with glue onto a spine and separate cover.
The utility pdfbook2 supports signatures with the --signature=INT switch where INT defaults to 4.  I've successfully used white PVA wood glue for this.  Hot glue is also an option if a suitable application method can be devised.  A glue gun and an iron works but don't get hot glue on the iron!

=== Binding

This can as simple as folding and stapling with a suitable stapler.  Other ways are comb binding, spiral binding, inserting into a clip binder or using something more akin to a traditional book.  There are many internet resources about book binding so there's really no need to cover them here.
