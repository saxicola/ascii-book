[role="center"]
Published by Michael F. Evans at Smashwords

[role="center"]
(C) 2016 Michael F. Evans

[role="center"]
Smashwords Edition

[role="center"]
*Smashwords Edition, License Notes*
Thank you for downloading this ebook. This book remains the copyrighted property of the author, and may not be redistributed to others for commercial or non-commercial purposes. If you enjoyed this book, please encourage your friends to download their own copy from their favourite authorized retailer. Thank you for your support.
